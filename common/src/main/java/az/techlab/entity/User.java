package az.techlab.entity;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "display-name")
    private String displayName;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;
}
